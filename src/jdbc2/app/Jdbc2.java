/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbc2.app;

import java.io.IOException;
import java.util.ArrayList;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import jdbc2.model.Aluno;
import jdbc2.model.FuncAdm;
import jdbc2.model.Pessoa;
import jdbc2.model.Professor;

/**
 *
 * @author Aluno
 */
public class Jdbc2 extends Application {

    static Stage stage ;

    public static void main(String[] args) {

        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        Jdbc2.stage = stage;
        Parent root = FXMLLoader.load(getClass().getResource("tableView.fxml"));

        Scene scene = new Scene(root);

        stage.setScene(scene);
        stage.show();
    }

    /*public ArrayList mostrar(){
        
       return listaPessoa;
    }*/
    public void trocaTela(String s) throws IOException {

        FXMLLoader loader = new FXMLLoader(getClass().getResource(s + ".fxml"));
        Parent root = loader.load();
        Scene scene = new Scene(root);
        Jdbc2.stage.setScene(scene);
        
        Jdbc2.stage.show();
    }
    /* public void adicionaPessoa (Pessoa pessoa){
         listaPessoa.add(pessoa);
     }*/

}
