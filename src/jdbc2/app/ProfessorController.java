package jdbc2.app;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import jdbc2.model.Pessoa;
import jdbc2.model.Professor;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Emilly Zoldan
 */
public class ProfessorController implements Initializable {

     public static Professor dado = new Professor();
     
    @FXML
    private TextField nomeTf;
    @FXML
    private TextField enderecoTf;
    @FXML
    private TextField idadeTf;
    @FXML
    private TextField disciplinaTf;
    
    @FXML
    private TextField salarioTf;
    @FXML
    private TextField idProf;
   
            
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        
        String idade = dado.getIdade()!= 0 ? Integer.toString(dado.getIdade()) : "" ;
        String salario = dado.getSalario()!= 0 ? Double.toString(dado.getSalario()) : "" ;
        String id = dado.getCod_pessoa() != 0 ? Integer.toString(dado.getCod_pessoa()) : "";
        
        
        nomeTf.setText(dado.getNome());
        enderecoTf.setText(dado.getEndereco());
        idadeTf.setText(idade);
        disciplinaTf.setText(dado.getDisciplina());
        salarioTf.setText(salario);
        idProf.setText(id);
        
        
    }
    @FXML
     public void cadastrar() throws IOException{    
        
       dado = new Professor();
        setDados();
       dado.setFuncaoP("professor");
        
        Pessoa.listaPessoa.add(dado);
        //   aluno.inserir();
        Jdbc2 jdbc = new Jdbc2(); 
        jdbc.trocaTela("tableView");
   
}
      public void mudar () throws IOException{
       
        setDados();
        Jdbc2 jdbc = new Jdbc2(); 
        jdbc.trocaTela("tableView");
        
}
    
    private void setDados(){
        
        dado.setNome(nomeTf.getText());
        dado.setIdade(Integer.parseInt(idadeTf.getText()));
        dado.setEndereco(enderecoTf.getText());
        dado.setSalario(Double.parseDouble(salarioTf.getText()));
        dado.setDisciplina(disciplinaTf.getText());
        dado.setCod_pessoa(Integer.parseInt(idProf.getText()));
    }
}