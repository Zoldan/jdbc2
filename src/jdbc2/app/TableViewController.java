/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbc2.app;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import jdbc2.model.Aluno;
import jdbc2.model.Conexao;
import jdbc2.model.FuncAdm;
import jdbc2.model.Pessoa;
import static jdbc2.model.Pessoa.listaPessoa;
import jdbc2.model.Professor;

/**
 * FXML Controller class
 *
 * @author Aluno
 */
public class TableViewController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    private TableView<Pessoa> tabela;
    @FXML
    private TableColumn<Pessoa, String> nome;
    @FXML
    private TableColumn<Pessoa, Integer> idade;
    @FXML
    private TableColumn<Pessoa, String> endereco;
    @FXML
    private TableColumn<Pessoa, String> funcao;
    @FXML
    private Label dadosL;
    @FXML
    private Label nomeL;
    @FXML
    private Label idadeL;
    @FXML
    private Label enderecoL;
    @FXML
    private Label funcaoL;
    @FXML
    private Label especialL;
    @FXML
    private Label especial2L;
    @FXML
    private Label especial3L;
   

    private ObservableList<Pessoa> ps;
    ArrayList<Pessoa> pessoas= new ArrayList<>();
    static ArrayList<Pessoa> auxiliar = new ArrayList<>();

    private static int x = 0;

    public void inicializarPessoas() {

        Professor prof = new Professor();
        prof.setNome("Bigolin");
        prof.setIdade(30);
        prof.setEndereco("Igara blabla perto do shop");
        prof.setDisciplina("programação");
        prof.setFuncaoP("professor");
        prof.setSalario(800);
        prof.setCod_pessoa(1);
        prof.getListaPessoa().add(prof);

        Professor prof2 = new Professor();
        prof2.setNome("Mariana");
        prof2.setEndereco("N faco ideia");
        prof2.setIdade(35);
        prof2.setDisciplina("matemática");
        prof2.setFuncaoP("professor");
        prof2.setSalario(900);
        prof2.setCod_pessoa(2);
        prof2.getListaPessoa().add(prof2);

        Professor prof3 = new Professor();
        prof3.setNome("Daniela");
        prof3.setIdade(32);
        prof3.setEndereco("bah, n sei");
        prof3.setDisciplina("química");
        prof3.setFuncaoP("professor");
        prof.setSalario(1000);
        prof.setCod_pessoa(3);
        prof3.getListaPessoa().add(prof3);

        Aluno aluno = new Aluno();
        aluno.setNome("Luis Felipe");
        aluno.setIdade(17);
        aluno.setEndereco("Esteio");
        aluno.setCurso("Desenvolvimento d S.");
        aluno.setSemestre("sexto");
        aluno.setFuncaoP("aluno");
        aluno.setCod_pessoa(4);
        aluno.getListaPessoa().add(aluno);

        Aluno aluno2 = new Aluno();
        aluno2.setNome("Emilly Zoldan");
        aluno2.setIdade(18);
        aluno2.setEndereco("Canoas");
        aluno2.setCurso("Desenvolvimento d S.");
        aluno2.setSemestre("sexto");
        aluno2.setFuncaoP("aluno");
        aluno2.setCod_pessoa(5);
        aluno2.getListaPessoa().add(aluno2);

        Aluno aluno3 = new Aluno();
        aluno3.setNome("Gloria M. G.");
        aluno3.setIdade(18);
        aluno3.setEndereco("Canoas");
        aluno3.setCurso("Desenvolvimento d S.");
        aluno3.setSemestre("sexto");
        aluno3.setFuncaoP("aluno");
        aluno3.setCod_pessoa(6);
        aluno3.getListaPessoa().add(aluno3);

        FuncAdm fa = new FuncAdm();
        fa.setNome("Maria");
        fa.setIdade(45);
        fa.setEndereco("rua tal tal");
        fa.setFuncao("faxineira");
        fa.setSetor("A");
        fa.setCod_pessoa(7);
        fa.setSalario(1000);
        fa.getListaPessoa().add(fa);

        FuncAdm fa2 = new FuncAdm();
        fa2.setNome("Joao");
        fa2.setIdade(52);
        fa2.setEndereco("rua tal tal");
        fa2.setFuncao("tesoureiro");
        fa2.setSetor("B");
        fa2.setCod_pessoa(8);
        fa2.setSalario(1000);
        fa2.getListaPessoa().add(fa2);

        FuncAdm fa3 = new FuncAdm();
        fa3.setNome("Pedro");
        fa3.setIdade(60);
        fa3.setEndereco("rua tal tal");
        fa3.setFuncao("psicólogo");
        fa3.setSetor("C");
        fa3.setCod_pessoa(9);
        fa3.setSalario(1000);
        fa3.getListaPessoa().add(fa3);

        x = 1;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        ps = tabela.getItems();
        nome.setCellValueFactory(new PropertyValueFactory<>("nome"));
        idade.setCellValueFactory(new PropertyValueFactory<>("idade"));
        endereco.setCellValueFactory(new PropertyValueFactory<>("endereco"));
        funcao.setCellValueFactory(new PropertyValueFactory<>("funcao"));
        this.tabela.setItems(ps);

        if (x != 1) {
            this.inicializarPessoas();
        }
        for (int i = 0; i < Pessoa.listaPessoa.size(); i++) {
            ps.add(Pessoa.listaPessoa.get(i));
        }
    }

    public void clickAluno() throws IOException {
        Jdbc2 jdbc = new Jdbc2();
        AlunoController.dado =  new Aluno();
        jdbc.trocaTela("aluno");
    }
    public void clickProfessor() throws IOException{
    Jdbc2 jdbc = new Jdbc2();
    ProfessorController.dado =  new Professor();
        jdbc.trocaTela("professor");
        
    }
    
    public void clickFuncAdm () throws IOException {
          Jdbc2 jdbc = new Jdbc2();
          FuncAdmController.dado =  new FuncAdm();
        jdbc.trocaTela("funcAdm");
    }
    
    /**
     *
     * @return
     * @throws IOException
     */
    public void modificar () throws IOException{
        
        Pessoa selectedItem = tabela.getSelectionModel().getSelectedItem();
        System.out.println(selectedItem);
        if(selectedItem instanceof Aluno){
            AlunoController.dado = (Aluno) selectedItem;
            Jdbc2 jb = new Jdbc2();           
           jb.trocaTela("aluno");   
        }
        
        if (selectedItem instanceof Professor){
            ProfessorController.dado = (Professor) selectedItem;
            Jdbc2 jb = new Jdbc2();           
            jb.trocaTela("professor");
        }
        
        if (selectedItem instanceof FuncAdm){
            FuncAdmController.dado = (FuncAdm) selectedItem;
            Jdbc2 jb = new Jdbc2();           
            jb.trocaTela("funcAdm");
            
        }
        
        auxiliar.add(selectedItem);
        System.out.println();
           
        
    }
    
    public void delete (){
        
        Pessoa selectedItem = tabela.getSelectionModel().getSelectedItem();
        
        tabela.getItems().remove(selectedItem);     
    }
    
    public void visualizar (){
        
        Pessoa selectedItem = tabela.getSelectionModel().getSelectedItem();
        if (selectedItem instanceof Aluno){
            
            AlunoController.dado = (Aluno) selectedItem;
            dadosL.setText("Dados do Aluno: ");
            nomeL.setText("Nome: "+AlunoController.dado.getNome());
            idadeL.setText("Idade: "+Integer.toString(AlunoController.dado.getIdade()));
            enderecoL.setText("Endereço: "+AlunoController.dado.getEndereco());
            funcaoL.setText("Função: Aluno");
            especialL.setText("Semestre: "+AlunoController.dado.getSemestre());
            especial2L.setText("Curso: "+AlunoController.dado.getCurso());         
        }
        
        if (selectedItem instanceof Professor){
            
            ProfessorController.dado = (Professor) selectedItem;
            dadosL.setText("Dados do Professor: ");
            nomeL.setText("Nome: "+ProfessorController.dado.getNome());
            idadeL.setText("Idade: "+Integer.toString(ProfessorController.dado.getIdade()));
            enderecoL.setText("Endereço: "+ProfessorController.dado.getEndereco());
            funcaoL.setText("Função: Professor");
            especialL.setText("Salário: "+Double.toString(ProfessorController.dado.getSalario()));
            especial2L.setText("Disciplina: "+ProfessorController.dado.getDisciplina());         
            
        }
        
        if (selectedItem instanceof FuncAdm){
            
            FuncAdmController.dado = (FuncAdm) selectedItem;
            dadosL.setText("Dados do Funcionário: ");
            nomeL.setText("Nome: "+FuncAdmController.dado.getNome());
            idadeL.setText("Idade: "+Integer.toString(FuncAdmController.dado.getIdade()));
            enderecoL.setText("Endereço: "+FuncAdmController.dado.getEndereco());
            funcaoL.setText("Função: "+FuncAdmController.dado.getFuncao());
            especialL.setText("Salário: "+Double.toString(FuncAdmController.dado.getSalario()));
            especial2L.setText("Setor: "+FuncAdmController.dado.getSetor());
            
        }
        
    }
    
    /*public void clickTabela () throws IOException{
        
        Pessoa selectedItem = tabela.getSelectionModel().getSelectedItem();
        String nome = selectedItem.getNome();
      
        String selectSQL = "SELECT * FROM OO_PessoaJDBC2 where nome = ?";
       
      
        Conexao conexao = new Conexao();
        Connection dbConnection = conexao.getConexao();
        PreparedStatement ps;
        
        if (selectedItem instanceof Aluno){
            
        try {
            ps = dbConnection.prepareStatement(selectSQL);
            ps.setString(1, nome);
            
            ResultSet rs = ps.executeQuery(selectSQL);
            while (rs.next()) {
                Aluno aluno = new Aluno();
                aluno.setNome(rs.getString("nome"));
                aluno.setIdade(rs.getInt("idade"));
                aluno.setCod_pessoa(rs.getInt("cod_pessoa"));
                aluno.setCurso(rs.getString("curso"));
                aluno.setEndereco(rs.getString("endereco"));
                aluno.setFuncaoP(rs.getString("funcaop"));
                aluno.setFuncaoP(rs.getString("funcaop"));
                aluno.setSemestre(rs.getString("semestre"));
                aluno.setCurso(rs.getString("curso"));
                pessoas.add(aluno);
                dadosL.setText("DADOS:");
                nomeL.setText(aluno.getNome());
                idadeL.setText(Integer.toString(aluno.getIdade()));
                enderecoL.setText(aluno.getEndereco());
                funcaoL.setText(aluno.getFuncao());
                especialL.setText(aluno.getCurso());
                especial2L.setText(aluno.getSemestre());
            }
          

        } catch (SQLException e) {
            e.printStackTrace();
        }
     
    } else if (selectedItem instanceof Professor){
        try {
            ps = dbConnection.prepareStatement(selectSQL);
            ps.setString(1, nome);
            
            ResultSet rs = ps.executeQuery(selectSQL);
            while (rs.next()) {
                Professor prof = new Professor();
                prof.setNome(rs.getString("nome"));
                prof.setIdade(rs.getInt("idade"));
                prof.setCod_pessoa(rs.getInt("cod_pessoa"));
                prof.setDisciplina(rs.getString("curso"));
                prof.setEndereco(rs.getString("endereco"));
                prof.setFuncaoP(rs.getString("funcaop"));
                prof.setSalario(rs.getDouble("salario"));
                pessoas.add(prof);
                dadosL.setText("DADOS:");
                nomeL.setText(prof.getNome());
                idadeL.setText(Integer.toString(prof.getIdade()));
                enderecoL.setText(prof.getEndereco());
                funcaoL.setText(prof.getFuncao());
                especialL.setText(prof.getDisciplina());
                especial2L.setText(Double.toString(prof.getSalario()));
            }
          

        } catch (SQLException e) {
            e.printStackTrace();
        }
    
    } else if (selectedItem instanceof FuncAdm){
        try {
            ps = dbConnection.prepareStatement(selectSQL);
            ps.setString(1, nome);
            
            ResultSet rs = ps.executeQuery(selectSQL);
            while (rs.next()) {
                FuncAdm fadm = new FuncAdm();
                fadm.setNome(rs.getString("nome"));
                fadm.setIdade(rs.getInt("idade"));
                fadm.setCod_pessoa(rs.getInt("cod_pessoa"));
                fadm.setSetor(rs.getString("curso"));
                fadm.setEndereco(rs.getString("endereco"));
                fadm.setFuncao(rs.getString("funcao"));
                fadm.setSalario(rs.getDouble("salario"));
                pessoas.add(fadm);
                dadosL.setText("DADOS:");
                nomeL.setText(fadm.getNome());
                idadeL.setText(Integer.toString(fadm.getIdade()));
                enderecoL.setText(fadm.getEndereco());
                funcaoL.setText(fadm.getFuncao());
                especialL.setText(fadm.getSetor());
                especial2L.setText(Double.toString(fadm.getSalario()));
            }
        
        
        
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    }
    */
}



    
    
      
    
        
        
        
        
        
    /*   
    
        
        String endereco = tabela.getSelectionModel().getSelectedItem().getEndereco();
        int idade = tabela.getSelectionModel().getSelectedItem().getIdade();
        String funcao = tabela.getSelectionModel().getSelectedItem().getFuncao();
        String funcaoP = tabela.getSelectionModel().getSelectedItem().getFuncaoP();
        int codg = tabela.getSelectionModel().getSelectedItem().getCod_pessoa();
        if (selectedItem instanceof Aluno){
        dadosL.setText("DADOS");
        nomeL.setText(nome);
            
     */   
    

