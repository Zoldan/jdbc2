/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbc2.app;

import java.io.IOException;
import static java.lang.System.out;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import static jdbc2.app.TableViewController.auxiliar;
import jdbc2.model.Aluno;
import jdbc2.model.FuncAdm;
import jdbc2.model.Pessoa;
import static jdbc2.model.Pessoa.listaPessoa;
import jdbc2.model.Professor;

/**
 * FXML Controller class
 *
 * @author Emilly Zoldan
 */
public class UpdateController implements Initializable {

    @FXML
    private Button nomeAB;
    @FXML
    private Button cursoAB;
    @FXML
    private Button idadeAB;
    @FXML
    private Button salarioFAB;
    @FXML
    private Button enderecoPB;
    @FXML
    private Button funcaoFAB;
    @FXML
    private Button setorFAB;
    @FXML
    private Button semestreAB;
    @FXML
    private Button disciplinaPB;
    @FXML
    private Button nomePB;
    @FXML
    private Button nomeFAB;
    @FXML
    private Button idadeFAB;
    @FXML
    private Button idadePB;
    @FXML
    private Button enderecoAB;
    @FXML
    private Button enderecoFAB;
    @FXML
    private Button salarioPB;
    @FXML
    private TextField profTF;
    @FXML
    private TextField funcATF;
    @FXML
    private TextField alunoTF;
    @FXML
    private Button modificar;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        alunoTF.setVisible(false);
        funcATF.setVisible(false);
        profTF.setVisible(false);
      
        if (auxiliar.get(0) instanceof Aluno) {

            nomePB.setVisible(false);
            idadePB.setVisible(false);
            enderecoPB.setVisible(false);
            disciplinaPB.setVisible(false);
            salarioPB.setVisible(false);
            profTF.setVisible(false);

            nomeFAB.setVisible(false);
            idadeFAB.setVisible(false);
            enderecoFAB.setVisible(false);
            funcaoFAB.setVisible(false);
            salarioFAB.setVisible(false);
            setorFAB.setVisible(false);
            funcATF.setVisible(false);

        } else if (auxiliar.get(0) instanceof Professor) {
            nomeAB.setVisible(false);
            idadeAB.setVisible(false);
            enderecoAB.setVisible(false);
            cursoAB.setVisible(false);
            semestreAB.setVisible(false);
            alunoTF.setVisible(false);

            nomeFAB.setVisible(false);
            idadeFAB.setVisible(false);
            enderecoFAB.setVisible(false);
            funcaoFAB.setVisible(false);
            salarioFAB.setVisible(false);
            setorFAB.setVisible(false);
            funcATF.setVisible(false);

        } else {
            nomeAB.setVisible(false);
            idadeAB.setVisible(false);
            enderecoAB.setVisible(false);
            cursoAB.setVisible(false);
            semestreAB.setVisible(false);
            alunoTF.setVisible(false);

            nomePB.setVisible(false);
            idadePB.setVisible(false);
            enderecoPB.setVisible(false);
            disciplinaPB.setVisible(false);
            salarioPB.setVisible(false);
            profTF.setVisible(false);

        }
        
    }

    public void onClickNome() {

        if (auxiliar.get(0) instanceof Aluno) {
            alunoTF.setVisible(true);
        }
        if (auxiliar.get(0) instanceof Professor) {
            profTF.setVisible(true);
        }
        if (auxiliar.get(0) instanceof FuncAdm) {
            funcATF.setVisible(true);
        }

    }

    public void onClickModificar() throws IOException {
        int x=0;
      
        if (auxiliar.get(0) instanceof Aluno) {
            String a = alunoTF.getText();
            
            //auxiliar.get(0).setNome(a);
            for (Pessoa p : listaPessoa){
                if (p == auxiliar.get(0)){
                    listaPessoa.get(x).setNome(a);
                }
                Jdbc2 jdbc2 = new Jdbc2 ();
                jdbc2.trocaTela("tableView");
                x++;
                 
            }
        }
    
        
          
            
        
            //listaPessoa.get(0)auxiliar.get(0).getNome());
        
        if (auxiliar.get(0) instanceof Professor) {

            String a = profTF.getText();
            auxiliar.get(0).setNome(a);
            System.out.println(auxiliar.get(0).getNome());
            profTF.setVisible(false);

        }
        if (auxiliar.get(0) instanceof FuncAdm) {

            String a = funcATF.getText();
            auxiliar.get(0).setNome(a);
            System.out.println(auxiliar.get(0).getNome());
            funcATF.setVisible(false);
        }
        auxiliar.remove(0);
    }
}
