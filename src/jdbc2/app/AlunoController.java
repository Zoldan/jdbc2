/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbc2.app;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import jdbc2.model.Aluno;
import jdbc2.model.Pessoa;

/**
 * FXML Controller class
 *
 * @author Emilly Zoldan
 */
public class AlunoController implements Initializable {

    public static Aluno dado = new Aluno();

    @FXML
    private TextField nomeTf;
    @FXML
    private TextField enderecoTf;
    @FXML
    private TextField idadeTf;
    @FXML
    private TextField semestreTf;
    @FXML
    private TextField cursoTf;
    @FXML
    private TextField idTf;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        String idade = dado.getIdade()!= 0 ? Integer.toString(dado.getIdade()) : "" ;
        String id = dado.getCod_pessoa() != 0 ? Integer.toString(dado.getCod_pessoa()) : "";
        
        
        nomeTf.setText(dado.getNome());
        enderecoTf.setText(dado.getEndereco());
        idadeTf.setText(idade);
        semestreTf.setText(dado.getSemestre());
        cursoTf.setText(dado.getCurso());
        idTf.setText(id);
        
        
    }    

    @FXML
    public void cadastrar () throws IOException{
        
        dado = new Aluno();
        setDados();
       dado.setFuncaoP("aluno");
        
        Pessoa.listaPessoa.add(dado);
        //   aluno.inserir();
        Jdbc2 jdbc = new Jdbc2(); 
        jdbc.trocaTela("tableView");
        
    }
    
    public void mudar () throws IOException{
       
        setDados();
        Jdbc2 jdbc = new Jdbc2(); 
        jdbc.trocaTela("tableView");
        
}
    
    private void setDados(){
        
        dado.setNome(nomeTf.getText());
        dado.setIdade(Integer.parseInt(idadeTf.getText()));
        dado.setEndereco(enderecoTf.getText());
        dado.setSemestre(semestreTf.getText());
        dado.setCurso(cursoTf.getText());
        dado.setCod_pessoa(Integer.parseInt(idTf.getText()));
    }
}
