/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbc2.app;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import jdbc2.model.FuncAdm;
import jdbc2.model.Pessoa;

/**
 *
 * @author Emilly Zoldan
 */
public class FuncAdmController implements Initializable {

    public static FuncAdm dado = new FuncAdm();
    
    @FXML
    private TextField nomeTf;
    @FXML
    private TextField enderecoTf;
    @FXML
    private TextField idadeTf;
    @FXML
    private TextField funcaoTf;
    @FXML
    private TextField salarioTf;
    @FXML
    private TextField setorTf;
    @FXML
    private TextField idTf;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
       
        String idade = dado.getIdade()!= 0 ? Integer.toString(dado.getIdade()) : "" ;
        String salario = dado.getSalario()!= 0 ? Double.toString(dado.getSalario()) : "" ;
        String id = dado.getCod_pessoa() != 0 ? Integer.toString(dado.getCod_pessoa()) : "";
        
        nomeTf.setText(dado.getNome());
        enderecoTf.setText(dado.getEndereco());
        idadeTf.setText(idade);
        funcaoTf.setText(dado.getFuncao());
        salarioTf.setText(salario);
        idTf.setText(id);
        setorTf.setText(dado.getSetor());
    }
    
    @FXML
    public void cadastrar () throws IOException{
        
        dado = new FuncAdm();
        setDados();
       dado.setFuncaoP("funcAdm");
        
        Pessoa.listaPessoa.add(dado);
        //   aluno.inserir();
        Jdbc2 jdbc = new Jdbc2(); 
        jdbc.trocaTela("tableView");
        
        /*FuncAdm fcadm = new FuncAdm();
        
        fcadm.setNome(nomeTf.getText());
        fcadm.setIdade(Integer.parseInt(idadeTf.getText()));
        fcadm.setEndereco(enderecoTf.getText());
        fcadm.setSalario(Double.parseDouble(salarioTf.getText()));
        fcadm.setFuncao(funcaoTf.getText());
        
        Pessoa.listaPessoa.add(fcadm);
       
        Jdbc2 jdbc = new Jdbc2(); 
        jdbc.trocaTela("tableView");
        */
    }
    
    public void mudar () throws IOException{
       
        setDados();
        Jdbc2 jdbc = new Jdbc2(); 
        jdbc.trocaTela("tableView");
        
}
    
    private void setDados(){
        
        dado.setNome(nomeTf.getText());
        dado.setIdade(Integer.parseInt(idadeTf.getText()));
        dado.setEndereco(enderecoTf.getText());
        dado.setSetor(setorTf.getText());
        dado.setFuncao(funcaoTf.getText());
        dado.setSalario(Double.parseDouble(salarioTf.getText()));
        dado.setCod_pessoa(Integer.parseInt(idTf.getText()));
    }
}


