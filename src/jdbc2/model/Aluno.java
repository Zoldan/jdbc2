/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbc2.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Statement;

/**
 *
 * @author Aluno
 */
public class Aluno extends Pessoa {
    
    private String semestre;
    private String curso;

    public String getSemestre() {
        return semestre;
    }

    public void setSemestre(String semestre) {
        this.semestre = semestre;
    }

    public String getCurso() {
        return curso;
    }

    public void setCurso(String curso) {
        this.curso = curso;
    }
    
  
    
    public void inserir() { // ok

        Conexao conexao = new Conexao();
        Connection dbConnection = conexao.getConexao();
        PreparedStatement preparedStatement = null;
        Statement st = null;
        String insertTableSQL = "INSERT INTO OO_PessoaJDBC2"
                + "(nome, idade, endereco, funcaoP, semestre, curso, cod_pessoa) VALUES"
                + "(?,?,?,?,?,?,?)"; // usa-se preparedstatement para n ocorrer sqlinjection

        try {

            preparedStatement = dbConnection.prepareStatement(insertTableSQL);
            preparedStatement.setString(1, this.getNome());
            preparedStatement.setInt(2, this.getIdade());
            preparedStatement.setString(3, this.getEndereco());
            preparedStatement.setString(4, this.getFuncao());
            preparedStatement.setString(5, this.getSemestre());
            preparedStatement.setString(6, this.getCurso());
            preparedStatement.setInt(7, this.getCod_pessoa());
            System.out.println(preparedStatement);

            //execute insert SQL Statement
            preparedStatement.executeUpdate();
            System.out.println("Inserido");
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        

    }
    
}
