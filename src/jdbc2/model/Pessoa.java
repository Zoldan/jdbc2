/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbc2.model;

import java.util.ArrayList;

/**
 *
 * @author Aluno
 */
public abstract class Pessoa {

    private int cod_pessoa;
    private String nome;
    private int idade;
    private String endereco;
    private String funcaoP;
    public static ArrayList<Pessoa> listaPessoa = new ArrayList<>();

    public ArrayList<Pessoa> getListaPessoa() {
        return listaPessoa;
    }

    public void setListaPessoa(ArrayList<Pessoa> listaPessoa) {
        this.listaPessoa = listaPessoa;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }


    public int getCod_pessoa() {
        return cod_pessoa;
    }

    public void setCod_pessoa(int cod_pessoa) {
        this.cod_pessoa = cod_pessoa;
    }

    public String getFuncao() {
        return funcaoP;
    }

    public void setFuncaoP(String funcaoP) {
        this.funcaoP = funcaoP;
    }
    
    

}
